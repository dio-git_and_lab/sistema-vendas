import inter.bootcamp.service.GreetingService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GreetingServiceTest {
    @Test
    void shouldReturnHelloWorld() {
        GreetingService greetingService = new GreetingService();

        assertThat(greetingService.sayHi())
                .isNotEmpty()
                .isEqualTo("Hello World");
    }

    @Test
    void shouldReturnHelloWorldWhenProvideNull() {
        GreetingService greetingService = new GreetingService();

        assertThat(greetingService.sayHi(null))
                .isNotEmpty()
                .isEqualTo("Hello World");
    }

    @Test
    void shouldReturnHelloMichael() {
        GreetingService greetingService = new GreetingService();

        assertThat(greetingService.sayHi("Michael"))
                .isNotEmpty()
                .isEqualTo("Hello Michael");
    }


}
